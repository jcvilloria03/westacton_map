<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AnnouncementsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function show()
    {
        return view('announcement');
    }
    public function addAnnouncement()
    {
        return view('announcement-add');
    }
    public function viewAnnouncement()
    {
        return view('announcement-view');
    }
    public function editAnnouncement($id)
    {
        return view('announcement-edit', ['id' => $id]);
    }

    public function delete($id)
    {
        $announcement = Announcement::find($id);
        $announcement->delete();

        return response(200);
    }

    public function get($id)
    {
        $announcement = Announcement::find($id);
        if (empty($announcement)) {
            abort(404, "Announcement not found");
        }

        return $announcement;
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'integer',
        ], [
            'title.required' => 'title is required',
            'content.required' => 'content is required',
        ]);

        if (!empty($inputs)) {
            if (!empty($inputs['startDate'])) {
                //$inputs['startDate'] = Carbon::parse($inputs['startDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['startDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['startDate'] = date('Y-m-d H:i:s',$time);
            }
            if (!empty($inputs['endDate'])) {
                //$inputs['endDate'] = Carbon::parse($inputs['endDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['endDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['endDate'] = date('Y-m-d H:i:s',$time);
            }
            try {
                $announcement = Announcement::find($id);
                $announcement->update($inputs);
            } catch (Throwable $e) {
                abort(500, $e->getMessage());
            }

        }

        return response(200);
    }

    public function create(Request $request)
    {
        $inputs = $request->all();

        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'integer',
        ], [
            'title.required' => 'title is required',
            'content.required' => 'content is required',
        ]);

        if (!empty($inputs)) {
            if (!empty($inputs['startDate'])) {
                //$inputs['startDate'] = Carbon::parse($inputs['startDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['startDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['startDate'] = date('Y-m-d H:i:s',$time);
            }
            if (!empty($inputs['endDate'])) {
                //$inputs['endDate'] = Carbon::parse($inputs['endDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['endDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['endDate'] = date('Y-m-d H:i:s',$time);
            }
            try {
                $announcement = Announcement::create($inputs);
                $announcement->save();
            } catch (Throwable $e) {
                abort(500, $e->getMessage());
            }
        }
        

        return response('Created', 204);
    }

    public function index(Request $request)
    {
        $request = request();

        $page = $request->input('page');
        $sortBy = $request->input('sort_by');
        $perPage = $request->input('per_page');
        $query = Announcement::where('title', '!=', null);
        if (!empty($sortBy)) {
            $sort = explode('.', $sortBy);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (!empty($request->input('group_by'))) {
            $query->groupBy($request->input('group_by'));
        }

        $query = $query->paginate(!empty($perPage) ? $perPage : $query->count(), ['*'], 'page', !empty($page) ? $page : 1);
        return $query;
    }
}
