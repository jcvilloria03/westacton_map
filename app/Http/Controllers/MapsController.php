<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MapsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function show()
    {
        return view('map');
    }
    public function addAnnouncement()
    {
        return view('announcement-add');
    }
    public function viewAnnouncement()
    {
        return view('announcement-view');
    }
    public function editAnnouncement($id)
    {
        return view('announcement-edit', ['id' => $id]);
    }

    public function delete($id)
    {
        $announcement = Announcement::find($id);
        $announcement->delete();

        return response(200);
    }

    public function get($id)
    {
        $announcement = Announcement::find($id);
        if (empty($announcement)) {
            abort(404, "Announcement not found");
        }

        return $announcement;
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'integer',
        ], [
            'title.required' => 'title is required',
            'content.required' => 'content is required',
        ]);

        if (!empty($inputs)) {
            if (!empty($inputs['startDate'])) {
                //$inputs['startDate'] = Carbon::parse($inputs['startDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['startDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['startDate'] = date('Y-m-d H:i:s',$time);
            }
            if (!empty($inputs['endDate'])) {
                //$inputs['endDate'] = Carbon::parse($inputs['endDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['endDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['endDate'] = date('Y-m-d H:i:s',$time);
            }
            try {
                $announcement = Announcement::find($id);
                $announcement->update($inputs);
            } catch (Throwable $e) {
                abort(500, $e->getMessage());
            }

        }

        return response(200);
    }

    public function create(Request $request)
    {
        $inputs = $request->all();

        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'integer',
        ], [
            'title.required' => 'title is required',
            'content.required' => 'content is required',
        ]);

        if (!empty($inputs)) {
            if (!empty($inputs['startDate'])) {
                //$inputs['startDate'] = Carbon::parse($inputs['startDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['startDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['startDate'] = date('Y-m-d H:i:s',$time);
            }
            if (!empty($inputs['endDate'])) {
                //$inputs['endDate'] = Carbon::parse($inputs['endDate'], "UTC")->setTimezone('Asia/Singapore');
                $mysubstr = substr($inputs['endDate'], 0, 33);
                $time = strtotime($mysubstr);
                $time = $time + (60 * 60 * 8);
                $inputs['endDate'] = date('Y-m-d H:i:s',$time);
            }
            try {
                $announcement = Announcement::create($inputs);
                $announcement->save();
            } catch (Throwable $e) {
                abort(500, $e->getMessage());
            }
        }
        

        return response('Created', 204);
    }

    public function index(Request $request)
    {
        $geoLocation = [];
        $request = request();

        $page = $request->input('page');
        $sortBy = $request->input('sort_by');
        $perPage = $request->input('per_page');
        $ips = $_SERVER;
        $ip = $_SERVER['REMOTE_ADDR'];
        //$ip = '136.158.34.14';
        $ip = '218.45.174.214';
        //$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        //var_dump($details);
        //die();
        
        $geoIP  = file_get_contents("http://www.geoplugin.net/php.gp?ip={$ip}");

//var_dump($geoIP);die();
        $str = substr($geoIP, strpos($geoIP, '"geoplugin_latitude";'));

        $tmp = explode(";", $str);

        $tmp2 = explode("\"", $tmp[1]);

        $geoLatitude = $tmp2[1];
        $tmp2 = explode("\"", $tmp[3]);
        $geoLongitude = $tmp2[1];

        $geoLocation['lat'] = (float)$geoLatitude;
        $geoLocation['lng'] = (float)$geoLongitude;
        $geoLocation['minlat'] = (float)$geoLatitude - 2;
        $geoLocation['minlng'] = (float)$geoLongitude + 2;
        $geoLocation['maxlat'] = (float)$geoLatitude + 2;
        $geoLocation['maxlng'] = (float)$geoLongitude - 2;


//$geoLocation['link'] = "http://www.openstreetmap.org/?lat=12.345&lon=12.345&zoom=17&layers=M";
        $geoLocation['link'] = "http://www.openstreetmap.org/export/embed.html?bbox=".$geoLocation['minlng'].",".$geoLocation['minlat'].",".$geoLocation['maxlng'].",".$geoLocation['maxlat']."&layer=mapnik&marker=".$geoLocation['lng'].",".$geoLocation['lat']; 
        
        //$geoLocation['link'] = "https://www.google.com/maps?q=14.586211,121.056585&hl=es&z=14&amp;output=embed";
        //$geoMap  = file_get_contents("https://master.apis.dev.openstreetmap.org/#map=15/{$geoLatitude}/{$geoLongitude}");
        //var_dump($geoMap);die();
        return $geoLocation;
    }

    public function input(Request $request)
    {
        $geoLocation = [];
        $request = request();

        $page = $request->input('page');
        $sortBy = $request->input('sort_by');
        $perPage = $request->input('per_page');
        
        $geoLatitude = $request->lat;
        $geoLongitude = $request->lng;

        $geoLocation['lat'] = (float)$geoLatitude;
        $geoLocation['lng'] = (float)$geoLongitude;
        $geoLocation['minlat'] = (float)$geoLatitude - 2;
        $geoLocation['minlng'] = (float)$geoLongitude + 2;
        $geoLocation['maxlat'] = (float)$geoLatitude + 2;
        $geoLocation['maxlng'] = (float)$geoLongitude - 2;

        $geoLocation['link'] = "http://www.openstreetmap.org/export/embed.html?bbox=".$geoLocation['minlng'].",".$geoLocation['minlat'].",".$geoLocation['maxlng'].",".$geoLocation['maxlat']."&layer=mapnik&marker=".$geoLocation['lng'].",".$geoLocation['lat'].",10"; 

        return $geoLocation;
    }
}
