## westacton_map

To get started, do the following:

- composer install
- npm install
- php artisan key:generate
- php artisan passport:install
- php artisan migrate
- php artisan serve
- access http://127.0.0.1:8000/home
- edit the file MapsController.php in \app\Http\Controllers<br/>
  a. if you are not using localhost or 127.0.0.1, edit public function index(Request $request) like this<br/>
     $ip = $_SERVER['REMOTE_ADDR'];<br/>
     //$ip = '136.158.34.14';<br/>
     //$ip = '218.45.174.214';<br/>
  b. if you are using localhost or 127.0.0.1 and want a Philippine IP, edit public function index(Request $request)<br/>
     //$ip = $_SERVER['REMOTE_ADDR'];<br/>
     $ip = '136.158.34.14';<br/>
     //$ip = '218.45.174.214';<br/>
  c. if you are using localhost or 127.0.0.1 and want a Japan IP, edit public function index(Request $request)<br/>
     //$ip = $_SERVER['REMOTE_ADDR'];<br/>
     //$ip = '136.158.34.14';<br/>
     $ip = '218.45.174.214';

