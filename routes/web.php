<?php

use App\Http\Controllers\BetsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

//Route::get('/home', 'AnnouncementsController@show')->name('announcement');
//Route::get('/add', 'AnnouncementsController@addAnnouncement')->name('announcement');
//Route::get('/edit/{id}', 'AnnouncementsController@editAnnouncement')->name('announcement');
//Route::get('/view/{id}', 'AnnouncementsController@viewAnnouncement')->name('announcement');

Route::get('/home', 'MapsController@show')->name('map');