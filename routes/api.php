<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'API\UsersController@login');
Route::post('/user/signup', 'API\UsersController@signUp');

//Route::get('/announcements', 'AnnouncementsController@index');
//Route::get('/announcement/{id}', 'AnnouncementsController@get');
//Route::post('/announcement', 'AnnouncementsController@create');
//Route::put('/announcement/{id}', 'AnnouncementsController@update');
//Route::delete('/announcement/{id}', 'AnnouncementsController@delete');

Route::get('/maps', 'MapsController@index');
Route::get('/input', 'MapsController@input');