@extends('layouts.app')

@section('title', 'Maps')

@section('content')

@guest
@else
<div class="row justify-content-left">
    <div class="col-md-12">
        <h1 class="title"></h1>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">

    </div>
</div>
<br>
<div class="row justify-content-center">
    <div class="col-md-12">
        <map-index :auth_user="{{Auth::user()}}"></map-index>
    </div>
</div>
@endguest

@endsection