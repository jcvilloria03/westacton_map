@extends('layouts.app')

@section('content')


@guest
@else
    <div class="row justify-content-left">
        <div class="col-md-12">
            <h1 class="title">Announcements Details</h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
        <b-button tag="a"
        type="is-info"
                href="/events">
                <b-icon pack="fas" icon="long-arrow-alt-left" size="is-small"> </b-icon
                    > <span> Back to List</span>
            </b-button>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
            <div class="col-md-12">
                <!--
                <announcement-view :id="{{$id}}" :auth_user="{{Auth::user()}}"></announcement-view>
                -->
            </div>
    </div>
@endguest

@endsection

