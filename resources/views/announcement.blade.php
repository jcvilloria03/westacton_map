@extends('layouts.app')

@section('title', 'Announcements')

@section('content')

@guest
@else
<div class="row justify-content-left">
    <div class="col-md-12">
        <h1 class="title"></h1>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <b-button rounded tag="a" type="is-info" href="/add">
            <b-icon pack="fas" icon="plus" size="is-small"> </b-icon> <span> Add </span>
        </b-button>
    </div>
</div>
<br>
<div class="row justify-content-center">
    <div class="col-md-12">
        <announcement-index :auth_user="{{Auth::user()}}"></announcement-index>
    </div>
</div>
@endguest

@endsection